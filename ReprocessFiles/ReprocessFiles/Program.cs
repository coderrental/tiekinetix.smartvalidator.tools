﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Common;
using Sandboxable.Microsoft.WindowsAzure.Storage;
using Sandboxable.Microsoft.WindowsAzure.Storage.Blob;
using static System.Configuration.ConfigurationSettings;

namespace ReprocessFiles
{
    class Program
    {
        private static readonly CloudStorageAccount StorageAccount = CloudStorageAccount.Parse(AppSettings.Get("storateConnectionString"));
        private static readonly CloudBlobClient BlobClient = StorageAccount.CreateCloudBlobClient();
        private static readonly CloudBlobContainer BlobContainer = BlobClient.GetContainerReference("userinputdata");
        static void Main(string[] args)
        {
            HandleParsingFailedFilesToReprocess();
            Log.Logger.Information("Done");
            Console.ReadKey();
        }
        public static void HandleParsingFailedFilesToReprocess()
        {
            var context = new DataClassesDataContext();
            if (!Guid.TryParse(AppSettings.Get("appId"), out var applicationId))
            {
                Log.Logger.Error("appId must be a string representing a Guid.");
                return;
            }
            var date = (DateTime) System.Data.SqlTypes.SqlDateTime.MinValue;
            if (AppSettings.Get("date") != "" && !DateTime.TryParse(AppSettings.Get("date"), out date))
            {
                Log.Logger.Error("date must be a string representing a DateTime.");
                return;
            }
            if (!int.TryParse(AppSettings.Get("status"), out var newStatus))
            {
                Log.Logger.Error("date must be a string representing an Integer.");
                return;
            }
            var xPath = AppSettings.Get("xPath");
            var keyValues = AppSettings.Get("keyValues").Split(',').ToList();
            var sourceBusinessKey = AppSettings.Get("sourceBusinessKey");
            var files = context.DataFiles.Where(x => x.Status == 20 && x.SourceBusinessKey == sourceBusinessKey && x.AppId == applicationId && x.UploadedTime > date).ToList();
            
            try
            {
                foreach (var file in files)
                {
                    var filePath = file.DataFileEvents.FirstOrDefault(x => x.DataFileEventTypeId == 0)?.FilePath;
                    if (filePath != null)
                    {
                        var fileContent = GetFileContent(filePath);
                        if (fileContent != "")
                        {
                            var document = XDocument.Parse(fileContent);
                            var key = document.XPathSelectElement(xPath)?.Value;
                            if (keyValues.Contains(key))
                            {
                                Log.Logger.Information($"DataFileId: {file.id}");
                                file.Status = newStatus;
                                context.SubmitChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Logger.Error(e, "Error");
            }
        }
        public static string GetFileContent(string fileName)
        {
            try
            {
                var blockBlob = BlobContainer.GetBlockBlobReference(fileName);
                blockBlob.FetchAttributes();
                using (var stream = new MemoryStream())
                {
                    blockBlob.DownloadToStream(stream);
                    return Encoding.UTF8.GetString(stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }
    }
}
