﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Common;
using CsvHelper;
using CsvHelper.Configuration;

namespace ExportCompositePreferenceValue
{
    class Program
    {
        private static Guid AppId => Guid.Parse("F609E523-80C7-4658-9A84-5D52309C2B22");

        static void Main(string[] args)
        {
            ExportCompositeRef();
        }

        private static void ExportCompositeRef()
        {
            var niteProdDb = new DataBaseDataContext(new SqlConnection("Server=tcp:validatorsql.database.windows.net,1433;Data Source=validatorsql.database.windows.net;Initial Catalog=smartvalidator_test_nitelze;User ID=smartvalidator@validatorsql;Password=open4me1234!@#$;MultipleActiveResultSets=True;TrustServerCertificate=False;Connection Timeout=180;"));
            var prodComposites = niteProdDb.CompositeReferences.Where(x => x.ApplicationId == AppId).ToList();
            
            foreach (var compositeReference in prodComposites)
            {
                var st = Stopwatch.StartNew();
                Log.Logger.Information($"Export composite reference {compositeReference.Name} ....");

                var lines = new List<string>();
                var str = string.Empty;

                var groups = compositeReference.CompositeReferenceValueGroups.OrderBy(x => x.Id).ToList();
                var records = GetRecords(compositeReference.id); //Get record include keys + values

                foreach (var field in compositeReference.CompositeReferenceFields.OrderBy(x => x.id))
                {
                    str += field.XPath.Split('/').LastOrDefault()?.Replace(":", "") + ",";
                }

                for (var i = 0; i < groups.Count; i++)
                {
                    str += groups[i].Name;
                    if (i < groups.Count - 1)
                        str += ",";
                }
                lines.Add(str);

                var index = 1;
                foreach (var record in records)
                {
                    str = string.Empty;
                    var currentKeys = record.CompositeReferenceKeys.Where(x => x.CompositeReferenceRecordId == record.Id).OrderBy(x => x.FieldId);
                    str += string.Join(",", currentKeys.Select(x => x.FieldValue)) + ",";

                    var values = record.CompositeReferenceValues.OrderBy(x => x.CompositeReferenceValueGroupId);
                    str += string.Join(",", values.Select(x => x.Value));
                    lines.Add(str);
                    Log.Logger.Information(index++.ToString());
                }
                CsvWriteFile(lines, $@"D:\{compositeReference.Name.Replace(" ", "")}.csv");
                st.Stop();
                Log.Logger.Information($"Finish in {st.Elapsed.Minutes}");
            }

            Console.Read();
        }

        private static List<CompositeReferenceRecord> GetRecords(Guid compositeId)
        {
            var ctx = new DataBaseDataContext(new SqlConnection("Server=tcp:validatorsql.database.windows.net,1433;Data Source=validatorsql.database.windows.net;Initial Catalog=smartvalidator_test_nitelze;User ID=smartvalidator@validatorsql;Password=open4me1234!@#$;MultipleActiveResultSets=True;TrustServerCertificate=False;Connection Timeout=180;"));
            var dataOptions = new System.Data.Linq.DataLoadOptions();
            dataOptions.LoadWith<CompositeReferenceRecord>(ca => ca.CompositeReferenceKeys);
            dataOptions.LoadWith<CompositeReferenceRecord>(ca => ca.CompositeReferenceValues);
            ctx.LoadOptions = dataOptions;

            var list = new List<CompositeReferenceRecord>();
            var num = 100;
            var index = 1;
            var count = ctx.CompositeReferenceRecords.Count(x => x.CompositeReferenceId == compositeId);

            Log.Logger.Information("Get records");
            while (list.Count < count)
            {
                var result = (from value in ctx.CompositeReferenceRecords
                              where value.CompositeReferenceId == compositeId
                              select value).Skip((index - 1) * num).Take(num);

                list.AddRange(result.ToList());
                Log.Logger.Information(list.Count + " /" + count);
                index++;
            }
            return list;
        }

        public static void CsvWriteFile(List<string> lines, string fileName)
        {
            var csvConfiguration = new CsvConfiguration
            {
                IgnoreReferences = true,
            };

            using (var csv = new CsvWriter(new StreamWriter(fileName), csvConfiguration))
            {
                foreach (var line in lines)
                {
                    var arr = line.Split(',');
                    foreach (var str in arr)
                    {
                        csv.WriteField(str);
                    }
                    csv.NextRecord();
                }
            }
        }
    }
}
