﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static System.Configuration.ConfigurationSettings;

namespace ImportCompositeReferenceValue
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Variable
            var dataAccess = new DataAccessDataContext();
            #endregion

            var newcompositeName = AppSettings.Get("newcompositeName");
            var oldCompositeName = AppSettings.Get("oldCompositeName");
            var composite = dataAccess.CompositeReferences.SingleOrDefault(x => x.Name == newcompositeName);
            var oldComposite = dataAccess.CompositeReferences.SingleOrDefault(x => x.Name == oldCompositeName);
            if (oldComposite == null)
            {
                Log.Logger.Information($"Not exist composite with name: {oldCompositeName}");
                return;
            }
            var shouldContinue = true;
            while (shouldContinue)
            {
                Log.Logger.Information($"Press 1 to import compositereference.\nPress 2 to update compositereference rule.\nPress 0 to Exit.");
                var choose = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                if (choose == 0)
                {
                    shouldContinue = false;
                }
                if (choose == 1)
                {
                    if (composite == null)
                    {
                        var newComposite = new CompositeReference
                        {
                            id = Guid.NewGuid(),
                            ApplicationId = oldComposite.ApplicationId,
                            Name = newcompositeName,
                            DocTypeId = oldComposite.DocTypeId,
                            ApplyAllTp = oldComposite.ApplyAllTp,
                            ApplyAllApp = oldComposite.ApplyAllApp
                            
                        };
                        var compositeByApp = new CompositeReferenceByApp
                        {
                            ApplicationId = newComposite.ApplicationId,
                            CompositeReferenceId = newComposite.id
                        };
                        dataAccess.CompositeReferences.InsertOnSubmit(newComposite);
                        dataAccess.CompositeReferenceByApps.InsertOnSubmit(compositeByApp);
                        dataAccess.SubmitChanges();
                        Import(dataAccess, newComposite);
                        
                    }
                    else
                    {
                        Log.Logger.Information($"Existing composite with name: {newcompositeName}");
                    }
                }
                else if (choose == 2)
                {
                    var newComposite = dataAccess.CompositeReferences.SingleOrDefault(x => x.Name == newcompositeName);
                    if(newComposite != null)
                    {
                        UpdateRule(dataAccess, oldComposite, newComposite);
                    }
                    else
                    {
                        Log.Logger.Information($"Not exist composite with name: {newcompositeName}");
                    }
                }
            }
            
            
            Console.ReadKey();
        }

        private static void Import(DataAccessDataContext dataAccess, CompositeReference composite)
        {
            var lines = System.IO.File.ReadAllLines(AppSettings.Get("filePath"));
            var groupNames = lines[0].Split(',').ToList();

            var xpaths = AppSettings.Get("xpath").Split(new string[]{"|"},StringSplitOptions.RemoveEmptyEntries).ToList();
            var listFields = new List<CompositeReferenceField>();
            try
            {
                //Log.Logger.Information("Deleting old data, take long time if there are many data...");
                //dataAccess.CompositeReferenceValueGroups.DeleteAllOnSubmit(composite.CompositeReferenceValueGroups);
                //UpdateRuleToOptional(dataAccess, composite.CompositeReferenceValueGroups.ToList());
                //dataAccess.CompositeReferenceFields.DeleteAllOnSubmit(composite.CompositeReferenceFields);
                //dataAccess.SubmitChanges();
                //dataAccess.CompositeReferenceRecords.DeleteAllOnSubmit(composite.CompositeReferenceRecords);
                //dataAccess.SubmitChanges();
                Log.Logger.Information("inserting...");
                foreach (var xpath in xpaths)
                {
                    if (!dataAccess.CompositeReferenceFields.Any(x =>
                        x.XPath == xpath && x.CompositeReferenceId == composite.id))
                    {
                        listFields.Add(new CompositeReferenceField
                        {
                            id = Guid.NewGuid(),
                            CompositeReferenceId = composite.id,
                            XPath = xpath
                        });
                    }
                }

                var listGroup = new List<CompositeReferenceValueGroup>();
                for (var i = listFields.Count; i < groupNames.Count; i++)
                {
                    var group = new CompositeReferenceValueGroup
                    {
                        Id = Guid.NewGuid(),
                        CompositeReferenceId = composite.id,
                        Name = groupNames[i].Trim().Replace("\"",string.Empty)
                    };
                    listGroup.Add(@group);
                    dataAccess.CompositeReferenceValueGroups.InsertOnSubmit(@group);
                }

                
                dataAccess.CompositeReferenceFields.InsertAllOnSubmit(listFields);
                dataAccess.SubmitChanges();

                var countFrom =1;

                for (var i = 1; i < lines.Length; i++)
                {
                    var record = new CompositeReferenceRecord {Id = Guid.NewGuid(), CompositeReferenceId = composite.id};
                   

                    var items = SplitCsv(lines[i]);

                    for (int k = 0; k < listFields.Count; k++)
                    {
                        record.CompositeReferenceKeys.Add(new CompositeReferenceKey
                        {
                            id = Guid.NewGuid(),
                            FieldId = listFields[k].id,
                            FieldValue = items[k].Trim().Replace("\"",string.Empty),
                        });
                    }

                    for (var j = listFields.Count; j < groupNames.Count; j++)
                    {
                        var value = "";
                        try
                        {
                            value = items[j];
                        }
                        catch (Exception e)
                        {
                            // ignored
                            value = "";
                        }
                        var groupvalue = listGroup.FirstOrDefault(x =>
                               x.Name == listGroup[j - listFields.Count].Name &&
                               x.CompositeReferenceId == listGroup[j - listFields.Count].CompositeReferenceId);
                        if (groupvalue == null)
                        {
                            continue;
                        }
                        record.CompositeReferenceValues.Add(new CompositeReferenceValue
                        {
                            id = Guid.NewGuid(),
                            Active = true,
                            CompositeReferenceValueGroupId = listGroup[j - listFields.Count].Id,
                            DefaultValue = false,
                            Value = value.Trim('"')
                        });
                    }
                    dataAccess.CompositeReferenceRecords.InsertOnSubmit(record);
                    if (i == countFrom + 400 || i == lines.Length -1)
                    {
                       
                        dataAccess.SubmitChanges();
                        Log.Logger.Information($"Insert record from {countFrom} to {countFrom + 400}");
                        countFrom += 400;
                    }

                    //Log.Logger.Information("insert record " + i);
                }
            }
            catch (Exception e)
            {
                Log.Logger.Error(e, "Error");
            }
        }

        private static void UpdateRule(DataAccessDataContext dataAccess, CompositeReference oldComposite, CompositeReference composite)
        {
            var oldCompositereferenceGroups = oldComposite.CompositeReferenceValueGroups.ToList();
            var newcompositereferenceGroups = composite.CompositeReferenceValueGroups.ToList();
            var rules = dataAccess.ValidationRules.Where(a => oldCompositereferenceGroups.Select(x => x.Id.ToString()).Contains(a.RuleValue)).ToList();
            foreach (var rule in rules)
            {
                var groupName = newcompositereferenceGroups.FirstOrDefault(a => a.Name == oldCompositereferenceGroups.First(x => x.Id.ToString() == rule.RuleValue).Name);
                if(groupName != null)
                    rule.RuleValue = groupName.Id.ToString();
            }
            dataAccess.SubmitChanges();
            Log.Logger.Information("Done!");
        }

        public static string[] SplitCsv(string input)
        {
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
            List<string> list = new List<string>();
            foreach (Match match in csvSplit.Matches(input))
            {
                var curr = match.Value;
                if (0 == curr.Length)
                {
                    list.Add("");
                }

                list.Add(curr.TrimStart(','));
            }

            return list.ToArray();
        }
    }
}
