﻿using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Common
{
    public class Log
    {
        public static readonly Logger Logger = new LoggerConfiguration()
            .WriteTo.Console(LogEventLevel.Information)
            .WriteTo.RollingFile("Log.txt")
            .CreateLogger();
    }
}
