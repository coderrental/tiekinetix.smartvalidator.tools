﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using static System.Configuration.ConfigurationSettings;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Common;

namespace UpdateIdentifier
{
    class Program
    {
        private  static readonly CloudStorageAccount StorageAccount = CloudStorageAccount.Parse(AppSettings.Get("storateConnectionString"));
        private static readonly CloudBlobClient BlobClient = StorageAccount.CreateCloudBlobClient();

        static void Main(string[] args)
        {
            var context = new DataClassesDataContext();
            var rangeDate = int.Parse(AppSettings.Get("dateRange"));
            var structureIdentifierIds = AppSettings.Get("structureIdentifierId").Split(',').ToList().ConvertAll(Guid.Parse);
            var sourceBusinessKey = AppSettings.Get("sourceBusinessKey");
            var applicationId = Guid.Parse(AppSettings.Get("appId"));

            var documentStructureIdentifiers = context.DocumentStructureIdentifiers.Where(x => structureIdentifierIds.Contains(x.Id)).ToList();
            var listDataFileStoredValues = new List<DataFileStoredValue>();
            var dataFileEvents = new List<DataFileEvent>();

            var docTypeId = context.DataSpecificationDocumentTypes.Where(x => x.BusinessKey == sourceBusinessKey).Select(x => x.id).First();
            var dataFileEventType = context.DataSpecificationDocumentTypeMiscs.FirstOrDefault(x =>
                                        x.ApplicationId == applicationId &&
                                        x.DataSpecificationDocumentTypeId == docTypeId &&
                                        x.IsUpdateIdentifier) != null
                ? 10
                : 0;

            var dataFiles = context.DataFiles.Where(dataFile =>
                dataFile.DataFileStoredValues.FirstOrDefault(a => a.StructureIdentifier != null) == null
                && dataFile.SourceBusinessKey == sourceBusinessKey &&
                dataFile.AppId == applicationId);
            if (rangeDate > 0)
            {
                dataFiles = dataFiles.Where(x => (DateTime.Now - x.UploadedTime).Days <= rangeDate);
            }

            foreach (var dataFile in dataFiles)
            {
                Log.Logger.Information($"datafile: {dataFile.id}");
                var dataFileEvent = context.DataFileEvents
                    .Where(x => x.DataFileEventTypeId == dataFileEventType && x.DataFileId == dataFile.id && x.Success)
                    .OrderBy(x => x.ExecutedTime).FirstOrDefault();
                if (dataFileEvent == null && dataFileEventType == 10)
                {
                    dataFileEvent = context.DataFileEvents
                        .Where(x => x.DataFileEventTypeId == 0 && x.DataFileId == dataFile.id && x.Success)
                        .OrderBy(x => x.ExecutedTime).FirstOrDefault();
                }

                if (dataFileEvent != null)
                {
                    dataFileEvents.Add(dataFileEvent);
                }
            }
            
            foreach (var dataFileEvent in dataFileEvents)
            {
                try
                {
                    Log.Logger.Information($"datafile event: {dataFileEvent.DataFileId}");
                    var fileContent = GetFileContent("userinputdata", dataFileEvent.FilePath);
                    if (fileContent == "")
                    {
                        continue;
                    }

                    var document = XDocument.Parse(fileContent);
                    foreach (var documentStructureIdentifier in documentStructureIdentifiers)
                    {
                        listDataFileStoredValues.Add(new DataFileStoredValue
                        {
                            Id = Guid.NewGuid(),
                            DataFileId = dataFileEvent.DataFileId,
                            StructureIdentifier = documentStructureIdentifier.Id,
                            Value = string.IsNullOrEmpty(documentStructureIdentifier.RegexXpath) ? document.XPathSelectElement(documentStructureIdentifier.XPath)?.Value : document.XPathSelectElement(documentStructureIdentifier.RegexXpath)?.Value
                        });
                    }
                }
                catch (Exception e)
                {
                    Log.Logger.Error(e, "Error");
                    break;
                }
            }
            context.DataFileStoredValues.InsertAllOnSubmit(listDataFileStoredValues);
            context.SubmitChanges();
            Log.Logger.Information("Done");
            Console.ReadKey();
        }
        public static string GetFileContent(string blobContainerName, string fileName)
        {
            try
            {
                var blobContainer = BlobClient.GetContainerReference(blobContainerName);
                blobContainer.CreateIfNotExists();
                var blockBlob = blobContainer.GetBlockBlobReference(fileName);
                blockBlob.FetchAttributes();
                using (var stream = new MemoryStream())
                {
                    blockBlob.DownloadToStream(stream);
                    return Encoding.UTF8.GetString(stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }
    }
}